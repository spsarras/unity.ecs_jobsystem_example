
# ECS Job System Example

This is a simple example that spawns multiple cubes using the new ECS Job System in Unity.

It is basically a transcription of [this video](https://www.youtube.com/watch?v=VgeVpCYzrSM&t=1727s)

Unity Version: 20118.3.0f2 

## Installation from Package Manager

- Entities
- Jobs
- Mathematics
- Burst
- Collections