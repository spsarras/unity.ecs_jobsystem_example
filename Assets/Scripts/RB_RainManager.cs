﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;

public class RB_RainManager : MonoBehaviour
{
    public int NumberOfEntitesToSpawn = 1000;
    public Text EntityNumberText;
    public int numberOfEnts = 0;

    public float max = 300;

    public GameObject SnowFlakeEnt;

    private void Start()
    {
        SpawnSnowflakes();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            SpawnSnowflakes();
    }


    private void SpawnSnowflakes()
    {
        EntityManager entityManager = World.Active.GetOrCreateManager<EntityManager>();
        NativeArray<Entity> snowFlake = new NativeArray<Entity>(NumberOfEntitesToSpawn, Allocator.Temp);
        entityManager.Instantiate(SnowFlakeEnt, snowFlake);

        for (int i = 0; i < NumberOfEntitesToSpawn; i++)
        {
            entityManager.AddComponentData(snowFlake[i], new RB_RotationSpeed { RotationValue = 0.5f });
            entityManager.AddComponentData(snowFlake[i], new RB_MoveDownSpeed { MoveSpeed = 1.0f });

            float3 startingPos = new float3(UnityEngine.Random.Range(-max, max), UnityEngine.Random.Range(10, max), UnityEngine.Random.Range(-max, max));
            entityManager.SetComponentData(snowFlake[i], new Position { Value = startingPos });
        }

        snowFlake.Dispose();

        numberOfEnts += NumberOfEntitesToSpawn;
        EntityNumberText.text = numberOfEnts.ToString();

    }
}

