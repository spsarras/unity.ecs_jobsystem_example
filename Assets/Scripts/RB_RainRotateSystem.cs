﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class RB_RainRotateSystem : JobComponentSystem
{
    private struct RainRotateJob: IJobProcessComponentData<RB_RotationSpeed, Rotation>
    {
        public float DeltaTime;

        public void Execute(ref RB_RotationSpeed speed, ref Rotation rot)
        {
            rot.Value = math.mul(math.normalize(rot.Value), quaternion.AxisAngle(math.up(), speed.RotationValue * DeltaTime));
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var job = new RainRotateJob
        {
            DeltaTime = Time.deltaTime
        };

        return job.Schedule(this, inputDeps);
    }
}
