﻿using UnityEngine;
using UnityEngine.UI;

public class SpawnBox : MonoBehaviour
{

    public int NumberOfEntities = 1000;
    public GameObject box;

    float RandX;
    float RandY;
    float RandZ;

    public Text NumberCount;

    public float max = 300;
    int numb = 0;

    // Start is called before the first frame update
    void Start()
    {
        SpawnBoxes();   
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            SpawnBoxes();
    }

    private void SpawnBoxes()
    {
        for (int i = 0; i < NumberOfEntities; i++)
        {
            RandX = Random.Range(-max, max);
            RandY = Random.Range(-max, max);
            RandZ = Random.Range(-max, max);

            Instantiate(box, new Vector3(RandX, RandY, RandZ), Quaternion.identity);
        }
        numb += NumberOfEntities;
        NumberCount.text = numb.ToString();
    }

}
