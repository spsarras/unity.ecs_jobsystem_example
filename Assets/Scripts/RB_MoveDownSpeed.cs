﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

public struct RB_MoveDownSpeed : IComponentData
{
    public float MoveSpeed;
}
