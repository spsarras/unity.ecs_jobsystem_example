﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBehaviour : MonoBehaviour
{

    float randomFall;
    float randomSpin;

    // Start is called before the first frame update
    void Start()
    {
        randomFall = Random.Range(0.1f, 0.2f);
        randomSpin = Random.Range(0.1f, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += Vector3.down * randomFall;
        this.transform.rotation *= Quaternion.AngleAxis(randomSpin, Vector3.up);
    }
}
