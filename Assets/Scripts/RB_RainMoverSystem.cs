﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

public class RB_RainMoverSystem : JobComponentSystem
{
    private struct RainMoveJob : IJobProcessComponentData<RB_MoveDownSpeed, Position>
    {
        public float DeltaTime;

        public void Execute(ref RB_MoveDownSpeed speed, ref Position pos)
        {
            pos.Value.y -= speed.MoveSpeed * DeltaTime;
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var job = new RainMoveJob
        {
            DeltaTime = Time.deltaTime

        };

        return job.Schedule(this, inputDeps);
    }

}
